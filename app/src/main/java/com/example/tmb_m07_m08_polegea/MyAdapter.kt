package com.example.tmb_m07_m08_polegea

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class MyAdapter(private val ParadesList:ArrayList<Parades>/*,private val listener:OnClickListener*/):RecyclerView.Adapter<MyAdapter.MyViewHolder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView=LayoutInflater.from(parent.context).inflate(R.layout.liniescardview,parent,false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return ParadesList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentItem=ParadesList[position]

        holder.idparada.text=currentItem.id.toString()
        holder.titolparada.text=currentItem.name
    }
    class MyViewHolder(itemView: View):RecyclerView.ViewHolder(itemView)
    {

        val idparada:TextView=itemView.findViewById(R.id.linianom)
        val titolparada:TextView=itemView.findViewById(R.id.ns)

    }
}