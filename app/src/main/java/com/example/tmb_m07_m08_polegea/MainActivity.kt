package com.example.tmb_m07_m08_polegea

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_TMB_M07_M08PolEgea_NoActionBar)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        replaceFragment(LlistaFragment())
    }

    private fun replaceFragment(homeFragment: Fragment) {
        val fragmentManager=supportFragmentManager
        val fragmentTransaction=fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.frame_layout,homeFragment)
        fragmentTransaction.setReorderingAllowed(true)
        fragmentTransaction.addToBackStack("torna")
        fragmentTransaction.commit()
    }
}